package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//"@SpringBootApplication" this is called as "Annotations" mark.
	//Annotations are used to provide supplemental information about the program
	//There are used to manage and configure the behavior of the framework


@SpringBootApplication
//Tells the spring boot that this will handle endpoints for web request.
@RestController
public class Wdc044Application {

	public static void main(String[] args) {
		//This method starts the whole Spring framework
		SpringApplication.run(Wdc044Application.class, args);
	}


	//This is used for mapping HTTP GET requests.
	@GetMapping("/hello")
	//"@RequestParam" is used to extract query parameters, form parameters and even files from request
		//name = john; Hello John
		// To append the URL with a name parameter we do the following:
		//http:localhost:8080/hello?name=john
		// ? means the start of the parameters followed by the "key-value" pairs
	public String hello(@RequestParam(value="name", defaultValue = "world") String name){
		return String.format("Hello %s", name);
	}

	@GetMapping("/greetings")
	public String greetings(@RequestParam(value="greet", defaultValue = "world") String name){
		return String.format("Good evening, %s! Welcome to Batch 265", name);
	}

	@GetMapping("/contactInfo")
	public String contactInfo(@RequestParam(value="name", defaultValue = "User") String name, @RequestParam(value = "email", defaultValue = "user@mail.com") String email){
		return String.format("Hello %s! Your email is %s.", name, email);
	}

	//Activity
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "user") String name){
		return String.format("Hi %s!", name);
	}

	@GetMapping("/nameAge")
	public String nameAge(@RequestParam(value = "name", defaultValue = "User") String name, @RequestParam(value = "age", defaultValue = "0") String age){
		return String.format("Hello %s! Your age is %s.", name, age);
	}
}
