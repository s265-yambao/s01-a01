package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.PostRepository;
import com.zuitt.wdc044.repositories.UserRepository;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class PostServiceImpl implements PostService{

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createPost(String stringToken, Post post){
        //findByUsername to retrieve the user
        //Criteria for finding the user is from jwtToken method (getUsernameFromToken(stringToken))
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Post newPost = new Post();
        //Title and content will come from the reqBody which is pass through "post" identifier

        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());

        //Author retrieved from the token
        newPost.setUser(author);
        //actual save the post
        postRepository.save(newPost);
    }

    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }

    //Edit a post
    public ResponseEntity updatePost(Long id, String stringToken, Post post){
        //we will retrieve the post for updating using the "id" provided
        Post postForUpdating = postRepository.findById(id).get();

        //Because relationship is established within the User and Post model, we are able to retrieve the username of the post owner
        String postAuthor = postForUpdating.getUser().getUsername();

        //We will retreive the username of the currently logged-in user in the token
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        //We will check if the currently logged-in user is the owner of the post
        if(authenticatedUser.equals(postAuthor)){
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);

            return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);

        }

        else {
            return new ResponseEntity<>("You are not authorized to edit this post", HttpStatus.UNAUTHORIZED);
        }
    }

    //Delete a Post
    public ResponseEntity deletePost(Long id, String stringToken){
        Post postForDeleting = postRepository.findById(id).get();
        String postAuthor = postForDeleting.getUser().getUsername();
        String authenticatedUSer = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUSer.equals(postAuthor)){
            postRepository.deleteById(id);

            return new ResponseEntity<>("Post deleted successfully", HttpStatus.OK);
        }
        else {
            return  new ResponseEntity<>("You are not Authorized to edit this post", HttpStatus.UNAUTHORIZED);
        }
    }

    public Iterable<Post> getAllUSerPost (String stringToken){
        String author = jwtToken.getUsernameFromToken(stringToken);
        User userId = userRepository.findByUsername(author);

        return userId.getPosts();
    }

}
